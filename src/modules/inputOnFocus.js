const inputOnFocus = (event) => {
    const target = event.target;
    const targetParent = target.parentNode;
    const placeholderCustom = targetParent.querySelector('.input__placeholder');
    if(targetParent.classList.contains('input__custom--focus') && placeholderCustom !== null) {
        targetParent.classList.remove('input__custom--focus');
    }
}

export default inputOnFocus;