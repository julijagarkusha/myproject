import overlayShow from "./overlayShow";
import overlayHidden from "./overlayShow";

const filterMenu = document.querySelector('.filter__list');
const filterToggle = () => {
    filterMenu.classList.add('filter__list--show');
    overlayShow();
};

export default filterToggle;