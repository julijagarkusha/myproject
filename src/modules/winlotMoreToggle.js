const winlotMoreToggle = (event) => {
    const target = event.target;
    target.classList.toggle('winLot__icon--hidden');
    const winLotMoreElement = target.parentNode.parentNode.parentNode.querySelector('.winLot__inner');
    winLotMoreElement.classList.toggle('winLot__inner--show');
};

export default winlotMoreToggle;