const inputFocus = (event) => {
    const target = event.target;
    const targetParent = target.parentNode;
    const placeholderCustom = targetParent.querySelector('.input__placeholder');
    if(targetParent.classList.contains('input__custom') && placeholderCustom !== null) {
        targetParent.classList.add('input__custom--focus');
    }
}

export default inputFocus;