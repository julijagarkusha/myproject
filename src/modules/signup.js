import { Validator } from 'livr'
import overlayShow from './overlayShow';
import overlayHidden from './overlayHidden';

const ERROR_ATTRIBUTE_NAME = 'data-error-message';
export const schema = {
  email: ['email', 'required'],
  password: ['string', 'required'],
  repeat_password: [{ equal_to_field: 'password' }, 'required'],
  agree_personal_data: [{'eq': 'on'}, 'required'],
}
export const errorMessages = {
  WRONG_EMAIL: 'Email введен не верно!',
  REQUIRED: 'Это поле обязательно!',
  NOT_ALLOWED_VALUE: 'Это поле обязательно!',
  FIELDS_NOT_EQUAL: 'Пароли не совпадают!',
}

export const form = document.getElementById('signup');
export const modal = document.getElementById('signup_success_modal');

export const parseFormData = formSelector => {
  const formData = new FormData(formSelector);
  const data = {};

  for (const key of formData.keys()) {
    data[key] = formData.get(key);
  }

  return data;
}

export const inputChangeHandler = key => () => {
  document.querySelector(`.signup #input_${key}`).removeAttribute(ERROR_ATTRIBUTE_NAME);
}

export const signupSubmitHandler = event => {
  event.preventDefault();
  Validator.defaultAutoTrim(true);
  const validator = new Validator(schema);
  const data = parseFormData(form);
  const validData = validator.validate(data);

  Object.keys(schema).forEach(key => {
    document.querySelector(`.signup #input_${key}`).removeAttribute(ERROR_ATTRIBUTE_NAME);
  });
  
  if (!validData) {
    const errors = validator.getErrors();
    Object.keys(errors).forEach((errorKey) => {
      const message = errorMessages[errors[errorKey]];
      document.querySelector(`.signup #input_${errorKey}`).setAttribute(ERROR_ATTRIBUTE_NAME, message);
    })
    return;
  }

  overlayShow();
  modal.classList.add('signupModal--show');
  console.log('debug validData', validData, typeof validData);
}

const signupInitEvent = () => {
  if (!form) {
    return;
  }
  form.addEventListener('submit', signupSubmitHandler)
  Object.keys(schema).forEach(key => {
    if (key === 'agree_personal_data') {
      document.querySelector(`.signup [name=agree_personal_data]`).addEventListener('click', inputChangeHandler(key))
    } else {
      document.querySelector(`.signup [name=${key}]`).addEventListener('keyup', inputChangeHandler(key));
    }
  });
  modal.addEventListener('click', event => {
    event.stopPropagation();
  });
  modal.querySelector('button').addEventListener('click', event => {
    event.preventDefault();
    modal.classList.remove('signupModal--show');
    overlayHidden();
  });
}

export default signupInitEvent;