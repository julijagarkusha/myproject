const overlayElem = document.querySelector('.overlay');

const overlayHidden = () => {
    overlayElem.classList.remove('overlay--show');
}

export default overlayHidden;