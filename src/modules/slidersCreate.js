import Glide from '@glidejs/glide';
import {isArray} from '@glidejs/glide/src/utils/unit';

class MobileCarousels  {
    constructor(selector, config = {
        type: 'carousel',
        perView: 1,
        focusAt: 0,
        gap: 20,
        dots: true,
        peek: {
            before: 0,
            after: 0,
        },
    }) {
        this.selectorQuery = selector;
        this.config = config;
    }

    status = false;
    sliderInstances = [];

    destroy() {
        this.status = false;
        this.sliderInstances.forEach(slider => {
            slider.destroy();
        });
        this.sliderInstances = [];
    }

    init() {
        this.status = true;
        document.querySelectorAll(this.selectorQuery).forEach((node) => {
            const slider = new Glide(node, this.config);
            slider.mount();
            this.sliderInstances.push(slider);
        })

    }
}

const carousels0 = new MobileCarousels('#cardRelevant');
const carousels1 = new MobileCarousels('#cardGrid');
const carousels2 = new MobileCarousels('#cardSold');
const carousels3 = new MobileCarousels('#addLotList1');
const carousels4 = new MobileCarousels('#addLotList2');
const winnersAuction = new MobileCarousels('#winnersAuction');
const carouselsDay1 = new MobileCarousels('#day0104');
const carouselsDay2 = new MobileCarousels('#day0204');
const carouselsDay3 = new MobileCarousels('#day0804');
const carouselsDay4 = new MobileCarousels('#day0904');

const mobilePointWidth = 768;

window.addEventListener('resize', (e) => {
    if (window.innerWidth < mobilePointWidth && !carousels0.status) {
        carousels0.init();
        carousels1.init();
        carousels2.init();
        carousels3.init();
        carousels4.init();
        winnersAuction.init();
        carouselsDay1.init();
        carouselsDay2.init();
        carouselsDay3.init();
        carouselsDay4.init();
    }

    if (window.innerWidth > mobilePointWidth && carousels0.status) {
        carousels0.destroy();
        carousels1.destroy();
        carousels2.destroy();
        carousels3.destroy();
        carousels4.destroy();
        winnersAuction.destroy();
        carouselsDay1.destroy();
        carouselsDay2.destroy();
        carouselsDay3.destroy();
        carouselsDay4.destroy();
    }
});

if (window.innerWidth < mobilePointWidth) {
    carousels0.init();
    carousels1.init();
    carousels2.init();
    carousels3.init();
    carousels4.init();
    winnersAuction.init();
    carouselsDay1.init();
    carouselsDay2.init();
    carouselsDay3.init();
    carouselsDay4.init();
}