const overlayElem = document.querySelector('.overlay');

const overlayShow = () => {
    overlayElem.classList.add('overlay--show');
}

export default overlayShow;