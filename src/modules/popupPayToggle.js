import overlayShow from "./overlayShow";
import overlayHidden from "./overlayShow";

const headerElem = document.querySelector('.header');
const popupPayElem = document.querySelector('.popup__pay');
const overlayElem = document.querySelector('.overlay');
const filterMenu = document.querySelector('.filter__list');
const filterIcon = document.querySelector('.filter__icon');

const popupPayToggle = (event) => {
    const target = event.target;
    headerElem.classList.remove('menu--show');
    if(filterIcon) {
        filterMenu.classList.remove('filter__list--show');
    }
    if (popupPayElem.classList.contains('popup--show')) {
        popupPayElem.classList.add('popup--hidden');
        overlayHidden();
        setTimeout(()=> {
            popupPayElem.classList.remove('popup--hidden');
            overlayElem.classList.remove('overlay--hidden');
            popupPayElem.classList.remove('popup--show');
            overlayElem.classList.remove('overlay--show');
        }, 300)
    } else {
        popupPayElem.classList.add('popup--show');
        overlayShow();
    }
}

export default popupPayToggle;