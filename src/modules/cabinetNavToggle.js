const cabinetNavMenu = document.querySelector('.cabinet aside');
import overlayShow from "./overlayShow";
import overlayHidden from "./overlayHidden";

const cabinetNavToggle = () => {
    if (cabinetNavMenu.classList.contains('show')) {
        cabinetNavMenu.classList.remove('show');
        overlayHidden();
    } else {
        cabinetNavMenu.classList.add('show');
        overlayShow();
    }
};

export default cabinetNavToggle;