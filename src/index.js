import $ from 'jquery';
import 'jquery-ui-dist/jquery-ui';
import 'jquery-ui-dist/jquery-ui.css';
// import 'jquery-ui/themes/base/accordion.css'
import 'jquery-mask-plugin';
import moment from 'moment';
import overlayShow from "./modules/overlayShow";
import overlayHidden from "./modules/overlayHidden";
import './styles/index.scss';
import './modules/slidersCreate';
import './modules/cabinetNavToggle';
import cabinetNavToggle from "./modules/cabinetNavToggle";
import winlotMoreToggle from "./modules/winlotMoreToggle";
import popupPayToggle from './modules/popupPayToggle';
import filterToggle from "./modules/filterToggle";
import inputFocus from "./modules/inputFocus";
import inputOnFocus from "./modules/inputOnFocus";
import signupInitEvent from './modules/signup';

const menuIconElem = document.querySelector('.menu--toggle');
const headerElem = document.querySelector('.header');
const popupPayElem = document.querySelector('.popup__pay');
const overlayElem = document.querySelector('.overlay');
const popupPayToggleElem = document.querySelector('.button__buy');
const cabinetNavIcon = document.querySelector('.cabinet__menu--toggle');
const winLotIcons = document.querySelectorAll('.winLot__icon');
const cabinetNavMenu = document.querySelector('.cabinet aside');
const filterMenu = document.querySelector('.filter__list');
const filterIcon = document.querySelector('.filter__icon');
const inputElements = document.querySelectorAll('input');
const modal = document.getElementById('signup_success_modal');

const menuMobileToggle = (event) => {
    const target = event.target;
    popupPayElem.classList.remove('popup--show');
    if(cabinetNavMenu) {
        cabinetNavMenu.classList.remove('show');
    }
    if(filterIcon) {
        filterMenu.classList.remove('filter__list--show');
    }
    overlayElem.classList.remove('overlay--show');
   if (headerElem.classList.contains('menu--show')) {
       headerElem.classList.add('menu--hidden');
       setTimeout(() => {
           headerElem.classList.remove('menu--show');
           headerElem.classList.remove('menu--hidden');
       }, 300)
   } else {
       headerElem.classList.toggle('menu--show');
   }
}

if(cabinetNavIcon) {
    cabinetNavIcon.addEventListener('click', cabinetNavToggle);
}

const overlayClick = (event) => {
    const target = event.target;
    overlayElem.classList.add('overlay--hidden');
    popupPayElem.classList.add('popup--hidden');
    if(filterIcon) {
        filterMenu.classList.remove('filter__list--show');
    }
    const timeout = setTimeout(()=> {
        popupPayElem.classList.remove('popup--hidden');
        overlayElem.classList.remove('overlay--hidden');
        popupPayElem.classList.remove('popup--show');
        overlayElem.classList.remove('overlay--show');
        modal.classList.remove('signupModal--show');
        if(cabinetNavMenu) {
            cabinetNavMenu.classList.remove('show');
        }
        clearTimeout(timeout);
        console.log('11112222')
    }, 300)
}

if(menuIconElem) {
    menuIconElem.addEventListener('click', menuMobileToggle);
}

if(popupPayToggleElem) {
    popupPayToggleElem.addEventListener('click', popupPayToggle);
}

if(filterIcon) {
    filterIcon.addEventListener('click', filterToggle)
}

overlayElem.addEventListener('click', overlayClick);

if(winLotIcons) {
    winLotIcons.forEach(winLotIcon => {
        winLotIcon.addEventListener('click', winlotMoreToggle);
    })
}

$('#cardNumber').mask('9999 9999 9999 9999');
$('#cardValidity').mask('99/99');

const interval = 1000;
[...$('.countdown')].forEach((item) => {
    const endTime = moment($(item).data('end-date')).format('x');

    setInterval(() => {
        const currentTime = moment().format('x');
        const duration = moment.duration(endTime - currentTime - interval, 'milliseconds');
        $(item).text(moment(duration.asMilliseconds()).format('DD[d]:HH[h]:mm[m]:ss[s]'));
    }, interval);
});

inputElements.forEach(inputElement => {
    inputElement.addEventListener('focus', inputFocus);
    inputElement.addEventListener('focusout', inputOnFocus);
})

signupInitEvent();

$('.accordion').accordion();